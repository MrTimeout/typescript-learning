#!/bin/bash
# execute in your shell temporarly: export PATH="$PATH:$PWD" && chmod 0700 tscgun.sh
# We can run a single ts file that way.
file=${1%%.ts}; version=${2:-"ES6"}; echo $file; tsc --target $version $file.ts && nodejs $file.js && rm $file.js