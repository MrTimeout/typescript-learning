
class Person {
  name: string;
}

const jill: {name: string} = {name: "Jill"};

const person: Person = jill;

console.log(person);

function greeting({name}: Person) {
  console.log("Hello ", name)
}

greeting(jill)
greeting(person)
// greeting({name: "pedro", surname: "other"}) // does not compile because the entry object doesn't fit the Person class