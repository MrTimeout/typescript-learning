// Return types on functions

function greeting_1(name: string): void {
  console.log(`Hello ${name}`)
}

function greeting_2(name: string) {
  console.log(`Hello ${name}`)
}

function sum(a: number, b: number): number {
  return a + b;
}

function sum_1(a: number, b: number) {
  return a + b;
}

type Runner = (a: number) => void;

const runner_1: Runner = (b: number) => console.log(b);

greeting_1("MrTimeout");
greeting_2("MrTimeout");

console.log(sum(1, 2));
console.log(sum(1, 2));