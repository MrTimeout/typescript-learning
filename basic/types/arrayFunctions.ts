// Array functions in ES6

namespace Es6Functions {
  interface Person {
    name: string,
    age: number,
  }

  const database: Array<Person> = [
    { name: "Carlos", age: 20 },
    { name: "Jhon", age: 30 },
    { name: "Lucas", age: 40 },
    { name: "Jhon", age: 50},
  ];

  // find
  const person: Person | undefined = database.find(item => item.name == "Jhon");
  const person1: Person | undefined = database.find(item => item.age > 60);

  console.assert(person?.name === database[1].name);
  console.assert(person1 === undefined);

  // filter
  const person2: Array<Person> = database.filter(item => item.age > 25);

  console.assert(person2.length == 3);

  // map
  database.map(item => { return { name: item.name.toUpperCase(), age: item.age } }).forEach(item => console.log(item));

  // reduce
  console.assert(database.map(item => item.age).reduce((acc, next) => acc + next, 0) == 140);

  // some
  console.assert(database.some(item => item.name.length > 4));

  // every
  console.assert(database.every(item => item.name.length > 2));

  // About Set
  const setA: Set<number> = new Set([1, 2, 3, 1, 2, 3, 1, 2, 3]);

  // transform to an array
  console.debug("forEach in Array.from array");
  Array.from(setA).forEach(item => console.debug(item));

  console.debug("forEach in spread operator");
  [...setA].forEach(item => console.debug(item));

  setA.add(4);
  setA.delete(4);
  console.assert(setA.has(1));
  console.assert(setA.size == 3);

  console.debug("keys()")
  for (const val of setA.keys()) {
    console.debug(val);
  }

  console.debug("values()");
  for (const val of setA.values()) {
    console.debug(val);
  }

  console.debug("entries()");
  for (const [key, val] of setA.entries()) {
    console.debug(key, val);
  }

  const employees: Map<String, Person> = new Map();
  const employees1: Map<String, Person> = database.reduce((map, person) => map.set(person.name, person), new Map());

  employees.set("1", { name: "lucas", age: 20 });
  employees.set("2", { name: "jhon", age: 23 });
  employees.set("3", { name: "Carlos", age: 24 });

  console.debug("Debugging employees1")
  for (const [id, person] of employees1) {
    console.debug(id, person);
  }
}
