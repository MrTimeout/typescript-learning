// Intersection and Union typescript types

// intersection
let obj: {name: string} & {age: number} = { name: "Lucas", age: 20 };
//let objj: {name: string} & {age: number} = { name: "Lucas" }; // can't be created because we need both properties to be present.

// union
let objj: {name: string} | {age: number} | null = { name: "lucas" }
let objjj: {name: string} | {age: number} | null = { age: 20 }
let objjjj: {name: string} | {age: number} | null = null