// Learning classes in typescript

class Person {
  name: string;

  constructor() {}

  speak() {
    console.log(`I am ${this.name}`);
  }
}

const person: Person = new Person();
person.name = "MrTimeout"; // we can access the property of the class freely. It is public.

person.speak();

// ECMAScript 2020 will support private fields via # symbol. However, only fields are supported, and it is a new standard that browser support is limited.
class Person1 {
  constructor(private name: string) {}

  speak() {
    console.log(`I am ${this.name}`);
  }
}

const person1: Person1 = new Person1("MrTimeout");
// person1.name = "Other"; // We can't update this property because it is private
person1.speak();

class Person2 {
  constructor(private readonly name: string) {}

  // We can't update the value of the property name because is readonly. Once it gets a value, it can't be changed.
  //x() {
  //  this.name = "other";
  //}

  speak() {
    console.log(`I am ${this.name}`)
  }
}

const person2: Person2 = new Person2("MrTimeout");

person2.speak();

// getters and setters are only available for ECMAScript 5 or higher so we need to modify the entry script adding --target "ES6"
class Person3 {
  constructor(private name: string) {}

  get Name() {
    return this.name.toUpperCase();
  }

  set Name(name: string) {
    this.name = name?.toLowerCase();
  }
}

const person3: Person3 = new Person3("mrtimeout");
person3.Name = "MrTimeout";
// person3.name = "other"; // We still can't access the name property because is private, but we can modify its value using the Set Name.

console.assert(person3.Name == "MRTIMEOUT");

// Static properties inside classes. This means that is a member of the class type, but not belongs to the instance.
class Person4 {
  static somethingHere: string = "Hello world from Person4 class";

  constructor() {}

  speak() {
    console.log(Person4.somethingHere);
  }
}

const person4: Person4 = new Person4();
// person4.somethingHere; // We can't access the static property using the instance because that value belongs to the type class and not to the instance.
person4.speak();
console.log(Person4.somethingHere);

// Interfaces
interface PersonInterface {
  name: string
  surname: string
  age: number
  greeting: () => string
}

const personInterface: PersonInterface = {
  name: "MrTimeout",
  surname: "MrTimeout",
  age: 24,
  greeting: (): string => `Name ${personInterface.name} and surname ${personInterface.surname} with an age of ${personInterface.age}`,
}

console.log(personInterface.greeting());

// Inheritance
class Animal {
  // When the property is set to protected, its value can be modified by another class extending from that one. If we are using private, we can't.
  constructor(protected name: string) {}
}

class Cat extends Animal {
  constructor() {
    super("cat");
  }
}

class Dog extends Animal {
  constructor() {
    super("Dog");
  }

  changeName() {
    this.name = this.name.toLowerCase();
  }
}

// Namespaces
namespace A {
  export class MyClass {
    constructor(private readonly value: string) {}

    something(): void {
      console.log(`Hello world. We have this value: ${this.value}`);
    }
  }
}

namespace B {
  class MyClass {
    constructor(private readonly value: string) {}
  }
}

// We can access the MyClass class inside the namespace A because it is exported.
const myClass: A.MyClass = new A.MyClass("some value here");

namespace Animals {
  export interface AnimalInterface {
    noise(): string;
  }

  export abstract class Animal implements AnimalInterface {
    constructor(private readonly name: string) {}

    abstract noise(): string;
  }

  export class Cat extends Animal {
    constructor() {
      super("Cat");
    }

    override noise(): string {
      return "meow";
    }
  }

  export class Dog extends Animal {
    constructor() {
      super("Dog");
    }

    override noise(): string {
      return "guau";
    }
  }
}

const cat: Animals.AnimalInterface = new Animals.Cat();
const dog: Animals.AnimalInterface = new Animals.Dog();
const chicken: Animals.AnimalInterface = {
  noise: function (): string {
    return "pio pio";
  },
};
/*
const owl: Animals.Animal = {
  name: "owl",
  noise: function (): string {
    return "owl noise";
  },
}
*/

console.log(cat.noise());
console.log(dog.noise());
console.log(chicken.noise());

namespace Interfaces {
  interface A {
    name: string;
  }

  interface B extends A {
    surname: string;
  }

  const a: A = {
    name: "this is the name a",
  }

  const b: B = {
    name: "this is the name b",
    surname: "this is the surname b",
  }

  console.log(a);
  console.log(b);
}

// generics
function getLength<T>(input: T): number | never {
  if (!(input as Object).hasOwnProperty("length")) {
    throw TypeError("Error trying to get length property");
  }
  return input["length"];
}

console.log(getLength("hello"));
console.log(getLength({ length: 20 }));

try {
  getLength(2);
} catch (e) {
  if (e instanceof TypeError) {
    console.log(e);
  }
}

// interesting generics
interface HasLength {
  length: number;
}

function getLength1<T extends HasLength>(input: T): number {
  return input.length;
}

console.log(getLength1("hello"));
console.log(getLength1({ length: 20 }));
// console.log(getLength1(2)); // at compile time it is already not working.

// Optional chaining
namespace OptionalChaining {
  interface A {
    name: string;
  }

  interface B {
    a?: A;
    other: string;
  }

  interface C {
    b?: B | null;
    something: string;
  }

  const c: C = {
    something: "something",
    b: {
      other: "other",
      a: {
        name: "name",
      },
    },
  };

  const cWithAUndefined: C = {
    something: "something",
    b: {
      other: "other",
      a: undefined,
    },
  };

  const cWithBNull: C = {
    something: "something",
    b: null,
  };

  // This is the optional chaining
  console.log(c?.b?.a?.name);
  console.log(cWithAUndefined?.b?.a?.name); // It will return undefined
  console.log(cWithBNull?.b?.a?.name); // It will return undefined, because b is null
}

// Nullish coalescing. It is a shorcut of the ternary operator.
namespace NullishCoalescing {
  type MyType = string | undefined | null;

  const a: MyType = undefined;
  const b: MyType = null;
  const c: MyType = "Hello world";
  const def4ult: string = "This is the default value";

  // If the value is null or undefined, it will pick up the default value.
  console.log(a ?? def4ult);
  console.log(b ?? def4ult);
  console.log(c ?? def4ult);
}