// Using the never type inside typescript

const rExp: RegExp = /[0-9]+/g;

function parseInteger(input: string): never | number {
  if (!rExp.test(input)) {
    throw TypeError(`Error parsing input ${input}`);
  }
  return Number(input);
}

parseInteger("10");

try {
  parseInteger("Hello");
} catch (e) {
  if (e instanceof TypeError) {
    console.log(e.message);
  }
}