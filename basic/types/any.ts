// this is the type "any". It accepts everything

const a: any = "Hello world";
const b: any = { name: "Lucas" };
const c: any = (a: number, b: number) => a + b;

console.log(c(1, 2));

let d: any = "Hello world";
d = new Array<string>();

// We can also execute the Array<T>.push action even if we don't tell the compiler which type it is.
(d as Array<string>).push("Hello");
d.push("world");

console.log(d.reduce((accumulator, next) => accumulator + " " + next), "")

try {
  let e: any = "Hello world";
  e = new Array<string>();

  // It will fail at runtime, not at compile time because this variable is of type any, so it accepts any value and the compiler
  // does not know the type of it until runtime comes into picture.
  e.doesnotexists();
  // This other option will throw an error at compile time because we are specifying the type of the variable
  // (e as Array<string>).doesnotexists();
} catch (f) {
  if (f instanceof TypeError) {
    console.log("This is a TypeError:", f.message);
  }
} finally {
  console.log("Take care and write safe code please. Thank you");
}