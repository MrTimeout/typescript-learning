// This is the unkown type

let aa: unknown = "Hello world";
aa = new Array<string>();

// Compiler complains of this line. We have to tell the compiler which type is this variable
// aa.push("Hello");

(aa as Array<string>).push("Hello");
if (aa instanceof Array<string>) {
  aa.push("world");
}

console.log((aa as Array<string>).reduce((acc, next) => acc + " " + next, ""));