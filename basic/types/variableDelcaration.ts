// We can declare a variable using:
//  var
//  const (ES6+)
//  let (ES6+)

namespace VariableDeclaration {
  //var case
  var a: number = 1;
  if (true) {
    var a: number = 2;
  }

  console.assert(a == 2);

  // var and let case
  var b: number = 1;
  if (true) {
    let b: number = 2;
    console.assert(b == 2);
  }

  console.assert(b == 1);

  // const case
  const c: string = "Hello";
  // c = "Bye"; // we can't reasign a value because it is typed as a string const.

  const d: string = "hey";
  if (true) {
    const d: string = "bye";
    console.assert(d == "bye");
  }

  console.assert(d == "hey");
}