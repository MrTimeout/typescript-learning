// Literal types

let bb: "a" | "b" | "c" | "d" | "e" = "a";
let cc: "a" | "b" | "c" | "d" | "e" = "b";
let dd: "a" | "b" | "c" | "d" | "e" = "c";
let ee: "a" | "b" | "c" | "d" | "e" = "d";
let ff: "a" | "b" | "c" | "d" | "e" = "e";
// let zz: "a" | "b" | "c" | "d" | "e" = "h";

let arr: Array<"a" | "b" | "c" | "d" | "e"> = [];
//arr.push("ee");

// Types to optimize code
type abcde = "a" | "b" | "c" | "d" | "e";

let bbb: abcde = "a";
let ccc: abcde = "b";
let ddd: abcde = "c";
let eee: abcde = "d";
let fff: abcde = "e";
// let zzz: abcde = "z";

let aarr: Array<abcde> = [];
// aarr.push("ee");