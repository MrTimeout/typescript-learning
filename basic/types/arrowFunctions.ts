// Learning about arrow functions and the this keyword

function greet1ng(name: string): void {
  // In strict mode, noImplicitThis will fail this line.
  // console.debug(this); // it is failing because of noImplicitThis.
  console.log(`Hello ${name}`);
}

// The father is the "window" in the browser. Here, it is the Node's global object.
greet1ng("MrTimeout");

// The caller is the function itself. The object that has already created
// When we try to create the function without a contructor declared, it will be of any type.
// const greet11ng: any = new greet1ng("MrTimeout"); //Just to clarify, only a function with a void return void can be called with the new keyword.

function sumEverything(...input: number[]): number {
  console.log(arguments.length);
  return input.reduce((acc, next) => acc + next, 0);
}

const sumEveryth1ng: (...input: number[]) => number = (...input: number[]): number => {
  // console.log(arguments); // arrow functions does not have an arguments collection.
  return input.reduce((acc, next) => acc + next, 0);
}

console.assert(sumEverything(1, 2, 3, 4) == 10);
console.assert(sumEveryth1ng(1, 2, 3, 4) == 10)

// Some syntax
namespace ArrowSyntax {
  // When we are in strict mode, we have to assign a type to each of the parameters used
  const sum = (first: number, second: number) => first + second;
  const sum_1 = (first: number, second: number) => (first + second);
  const sum_2 = (first: number, second: number) => { console.log(`Adding ${first} to ${second}`); return first + second; }
}

// Example of this problem with standard functions
namespace LearningAboutNoImplicitThis {
  interface Person {
    name: string;
    f(): () => string;
    ff(): () => string;
    fff(this: Person): () => string;
  }

  const a = {};
  const b: Person = {
    name: "MrTimeout",
    f: function (): () => string {
      return function (): string {
        // noImplicitThis -> This means that this can be of <any> type. Help: https://www.typescriptlang.org/tsconfig#noImplicitThis
        return this.name; // It fails in strict mode.
      }
    },
    ff: function (): () => string { // in this case, this an be of type any.
      return () => this.name;
    },
    fff: function(this: Person) { // this parameter
      return () => this.name;
    },
  }

  // Annotation: It won't work using strict mode
  // Here the "this" context is the Node's global object.
  console.assert(b.f()() == undefined);
  // Here we are telling the function that the "this" context, which we want to use is the one of the b object.
  console.assert(b.f().call(b) == "MrTimeout");

  // Here the "this" context is the actual b object because we are using arrow functions and it preserves the context when returning. That's awesome.
  console.assert(b.ff()() == 'MrTimeout', { errorMsg: "Standard ff with Person context failed" });
  // We can try to call the ff function using another context (like a) and it will return undefined because "a" doesn't have a name property. 
  console.assert(b.ff.call(a)() == undefined, { erroMsg: "Standard ff with a context failed"});

  // It will work as expected
  console.assert(b.fff()() == "MrTimeout", { errorMsg: "something"});
  console.assert(b.fff.call(a)() == undefined, "Paremeter \"this\" with type different from Person will fail at compile time");
}

namespace LearningAboutThisContext {
  const a = {
    name: "MrTimeout",
    f: function(): string {
      return `Hello ${this.name}`; // In that case, this is of any type, so we can pass whatever we want.
    },
  };
  const b = {
    name: "Other",
  };

  console.assert(a.f() == "Hello MrTimeout");

  // We are making a call to the function a.f using the object b. This call is of one shot. After its completion, the function will return the same value as before because the context keeps being the one that you pass into it(a).
  console.assert(a.f.call(b) == "Hello Other");
  console.assert(a.f() == "Hello MrTimeout");

  // We are making a call to the function a.f using the object b. This call is of one shot. After its completion, the function will return the same value as before because the context keeps being the one that you pass into it(a).
  console.assert(a.f.apply(b) == "Hello Other");
  console.assert(a.f() == "Hello MrTimeout");

  // It returns the function with the new context to use, in that case the b context.
  const newf = a.f.bind(b);
  console.assert(newf() == "Hello Other");
  console.assert(newf() == "Hello Other");
  console.assert(a.f() == "Hello MrTimeout"); // Anyway, it keeps the context inside the function where we have declared it.
}