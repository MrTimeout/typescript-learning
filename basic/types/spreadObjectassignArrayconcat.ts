// Learning about spread, Object.assign and Array.concat to merge more than one element.

namespace LearningSpread {
  const a = {
    value: "A",
    valuee: "AA",
  };
  const b = {
    value: "B",
    valueee: "BBB",
  };

  const c = {...a, ...b};
  console.assert(c.value == "B");
}

namespace LearningObjectAssign {
  class A {
    value: string = "A";
    valuee: string = "AA";
  };

  class B {
    value: string = "B";
    valueee: string = "B";
  };
  
  const a: A = new A();
  const b: B = new B();
  const c: A = Object.assign(a, b); // The type of the c is A, because we assign properties of B to A.
  const d: A & B = {...a, ...b};

  console.assert(c instanceof A);
  console.assert(!(c instanceof B));

  console.assert(!(d instanceof A));
  console.assert(!(d instanceof B));
}

namespace LearningArrayConcat {
  const a: Array<number> = [1, 2, 3];
  const b: Array<number> = [4, 5, 6];

  const c: Array<number> = [...a, ...b];
  const d: Array<number> = a.concat(b);

  console.log(a, b, c, d);

  a.push(10);
  b.push(19);

  console.log(a, b, c, d);
}

namespace LearningDestructuring {
  const {name: fullName, age} = function(): any {
    return { name: "MrTimeout", age: 22, other: "stuff"}
  }();
  const [first, second] = function(): Array<string> {
    return ["Hello", "world", "bye"];
  }();

  console.assert(fullName == "MrTimeout");
  console.assert(age == 22);

  console.assert(first == "Hello");
  console.assert(second == "world");
}